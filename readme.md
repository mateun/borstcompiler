# borst compiler

This is a compiler for a small toy language named "borst". 
Its main purpose is for learning and having some fun while experimenting with 
lexing, parsing and generating code. 

### How to build

#### Prerequisites
A reasonably modern c++ compiler, e.g. g++ version >= 5. 
The easiest and intended way to build borstc is with cmake (version >= 3.10).


Create a directory named "build" in the root folder of the project. 

The folder structure might then look like this: 

build/
src/
CMakeLists.txt
readme.md


Enter the "build" directory and type "cmake ..". 

You will then have make files created for you and you can trigger the actual build with: 

make

### Running borstc

To run borstc you just type borstc followed by a list of files which you want to compile. 



